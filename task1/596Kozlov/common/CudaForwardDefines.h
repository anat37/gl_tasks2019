#pragma once
#include <vector_types.h>
#include <glm/glm.hpp>

template <typename T>
class CudaResource;

template <typename T>
class CudaImage;

typedef CudaImage<glm::u8vec4> CudaImage4c;
