#pragma once

#include <glm/glm.hpp>
#include <common/common_functions.cuh>
#include "triangle.h"

void mesh_paint(const Triangle* mesh, int triangle_count, glm::u8vec4* dst_image, ImageWorkArea dstWorkArea, glm::uvec2 blockDim);

void mesh_paint2(const Triangle* mesh, int triangle_count, glm::u8vec4* dst_image, ImageWorkArea dstWorkArea, glm::uvec2 blockDim);
