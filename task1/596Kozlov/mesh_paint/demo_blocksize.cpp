#include <cudasoil/cuda_soil.h>
#include <common/CudaImage.h>
#include <common/CudaTimeMeasurement.h>
#include <common/CudaInfo.h>

#include <vector>

#include "mesh_paint.cuh"
#include "build_mesh.cuh"



int main() {
	printCudaInfo();
	cudaDeviceProp deviceProp;
	cudaGetDeviceProperties(&deviceProp, 0);

	//auto sourceImage = load_image_rgb("596KozlovData1/image_8k.png");
	auto dstImage = std::make_shared<CudaImage4c>(4000, 2000, ResourceLocation::Device);
	ImageWorkArea workArea(glm::uvec2(4000, 2000));


	struct TimeReport {
		float totalTime = 0.0f;
		size_t measurements = 0;
		glm::uvec2 blockSize;
	};

	std::vector<TimeReport> results;
	const int grid_x = 200;
	const int grid_z = 100;
	const int N = 2 * grid_x * grid_z;
	glm::uvec2 blockSize = { 1u << 3, 1u << 3 };
	CudaResource<Triangle> meshData(N * sizeof(Triangle), ResourceLocation::Device);
	build_mesh(meshData.getHandle(), grid_x, grid_z, blockSize);
	

	CudaTimeMeasurement timer;
	auto runTask = [&](glm::uvec2 blockSize, size_t measureId) {
		cudaMemset(dstImage->getHandle(), 0, dstImage->getSize());

		timer.start();
		mesh_paint(meshData.getHandle(), N, dstImage->getHandle(), workArea, blockSize);
		timer.stop();

		if (results.size() <= measureId) {
			results.resize(measureId + 1, {0.0f, 0, blockSize});
		}
		results[measureId].totalTime += timer.getSeconds();
		results[measureId].measurements++;
	};

	for (int measurement = 0; measurement < 25; measurement++) {
		size_t measureId = 0;

		for (unsigned int i = 3u; i < 10u; i++) {
			for (unsigned int j = 3u; j < 10u; j++) {
				glm::uvec2 blockSize = {1u << i, 1u << j};
				if (blockSize.x * blockSize.y > deviceProp.maxThreadsPerBlock) {
					break;
				}

				runTask(blockSize, measureId);

				++measureId;
			}
		}
	}

	for (const auto &result : results) {
		std::cout << "Grayscale:\tblock size\t" << result.blockSize.x << "x"
		<< result.blockSize.y << "\t\ttime " << result.totalTime / result.measurements << "s" << std::endl;
	}

	save_image_rgb("596KozlovData1/out_image_8k_grayscale.png", dstImage);
	system("pause");
	return 0;
}
