#pragma once

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

struct Triangle {
	glm::vec3 point1;
	glm::vec3 point2;
	glm::vec3 point3;
	glm::vec4 color1;
	glm::vec4 color2;
	glm::vec4 color3;
};