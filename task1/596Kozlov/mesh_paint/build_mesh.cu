#include <common/common_functions.cu>
#include <common/CudaError.h>

#include <common/CudaForwardDefines.h>

#include "triangle.h"
#include <glm/glm.hpp>

__device__ float get_y(float z, float x) {
	return x / 10. + ((int)z % 501);
}

// This function is executed on DEVICE and can be called both from HOST and DEVICE.
__global__ void _build_mesh(Triangle* mesh, int grid_x, int grid_z) {
	int i = threadIdx.y + blockDim.y * blockIdx.y;
	int j = threadIdx.x + blockDim.x * blockIdx.x;
	if (i >= grid_z) return;
	if (j >= grid_x) return;
	
	float z1 = 1000 * i / grid_z;
	float z2 = 1000 * (i + 1) / grid_z;
	float x1 = 4000 * j / grid_x;
	float x2 = 4000 * (j + 1) / grid_x;

	Triangle& trig1 = mesh[2 * (i*grid_x + j)];
	trig1.point1 = { x1, get_y(z2, x1), z2 };
	trig1.point2 = { x2, get_y(z1, x2), z1 };
	trig1.point3 = { x1, get_y(z1, x1), z1 };
	trig1.color1 = glm::vec4{ 255.,z2 / 3,0,255 };
	trig1.color2 = glm::vec4{ 255.,z1 / 3,0,255 };
	trig1.color3 = glm::vec4{ 255.,z1 / 3,0,255 };

	/*Triangle& trig2 = mesh[2 * (i*grid_x + j) + 1];
	trig2.point1 = { x1, get_y(z2, x1), z2 };
	trig2.point2 = { x2, get_y(z1, x2), z1 };
	trig2.point3 = { x2, get_y(z2, x2), z2 };
	trig2.color1 = trig1.color1;
	trig2.color2 = trig1.color2;
	trig2.color3 = glm::vec4{ 255.,z2 / 2,0,255 };
	*/
}

void build_mesh(Triangle* mesh, int grid_x, int grid_z, glm::uvec2 blockDim) {
	dim3 blkDim(blockDim.x, blockDim.y, 1);
	dim3 grdDim = { (grid_x + blockDim.x - 1) / blockDim.x, (grid_z + blockDim.y - 1) / blockDim.y, 1 };

	_build_mesh << <grdDim, blkDim >> > (mesh, grid_x, grid_z);
	cudaDeviceSynchronize();
	checkCudaErrors(cudaGetLastError());
}