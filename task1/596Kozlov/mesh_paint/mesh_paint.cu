#include <common/common_functions.cu>
#include <common/CudaError.h>

#include <common/CudaForwardDefines.h>

#include "triangle.h"
#include "common/CudaResource.h"
#include <glm/glm.hpp>

#include <thrust/device_ptr.h>

#include <thrust/fill.h>

#include <device_atomic_functions.h>


__device__ bool intersect(const Triangle* triangle, glm::ivec2& workPixel, double& distance, glm::vec4& color) {
	distance = INFINITY;
	const float epsilon = 1e-4;

	glm::vec3 p1 = { triangle->point1.x, triangle->point1.y, 0 };
	glm::vec3 p2 = { triangle->point2.x, triangle->point2.y, 0 };
	glm::vec3 p3 = { triangle->point3.x, triangle->point3.y, 0 };
	float area = glm::length(glm::cross(p2 - p1, p3 - p1));
	if (area < epsilon) return false;
	
	glm::vec3 point = glm::vec3{ workPixel.x, workPixel.y, 0 };
	float area1 = glm::length(glm::cross(point - p1, point - p2)) / area;
	float area2 = glm::length(glm::cross(point - p2, point - p3)) / area;
	float area3 = glm::length(glm::cross(point - p3, point - p1)) / area;
	
	distance = glm::length( triangle->point3 * area1 + triangle->point1 * area2 + triangle->point2 * area3);
	color = triangle->color3 * area1 + triangle->color1 * area2 + triangle->color2 * area3;

	return abs(area1 + area2 + area3 - 1.0f) < epsilon;	
}

// This function is executed on DEVICE and can be called both from HOST and DEVICE.
__global__ void _mesh_paint(const Triangle* mesh, int triangle_count, glm::u8vec4* dst_image, ImageWorkArea dstWorkArea) {
	glm::ivec2 workPixel(threadIdx.x + blockDim.x * blockIdx.x, threadIdx.y + blockDim.y * blockIdx.y);

	int dstOffset = count_img_offset(workPixel, dstWorkArea);

	if (dstOffset < 0)
		return;

	double minDistance = INFINITY;
	double distance;
	glm::vec4 color;
	for (int i = 0; i < triangle_count; ++i) {	
		if (intersect(&mesh[i], workPixel, distance, color)) {
			if (distance < minDistance && distance > 0) {
				dst_image[dstOffset] = glm::u8vec4(color);
				minDistance = distance;
			}
		}
	}
}

// This is wrapper function for calling DEVICE _grayscale function from HOST.
void mesh_paint(const Triangle* mesh, int triangle_count, glm::u8vec4* dst_image, ImageWorkArea dstWorkArea, glm::uvec2 blockDim) {
	dim3 blkDim(blockDim.x, blockDim.y, 1);
	glm::uvec2 gridDim = glm::uvec2(glm::ivec2(dstWorkArea.workDim + blockDim) - 1) / blockDim;
	dim3 grdDim = { gridDim.x, gridDim.y, 1 };

	const int chunk_size = 100;

	for (int i = 0; i < triangle_count; i+=chunk_size) {

		_mesh_paint << <grdDim, blkDim >> > (mesh + i, glm::min(chunk_size, triangle_count-i), dst_image, dstWorkArea);
	}
	cudaDeviceSynchronize();
	checkCudaErrors(cudaGetLastError());
}

__global__ void _mesh_paint2(const Triangle* mesh, unsigned int* mutex, float* zbuffer, glm::u8vec4* dst_image, ImageWorkArea dstWorkArea) {
	const Triangle& trig = mesh[blockIdx.x];

	int left = glm::min(glm::min(trig.point1.x, trig.point2.x), trig.point3.x);
	int right = glm::max(glm::max(trig.point1.x, trig.point2.x), trig.point3.x);
	int up = glm::min(glm::min(trig.point1.y, trig.point2.y), trig.point3.y);
	int down = glm::max(glm::max(trig.point1.y, trig.point2.y), trig.point3.y);

	for (int i = threadIdx.x; i <= right - left; i += blockDim.x) {
		for (int j = threadIdx.y; j <= down - up; j += blockDim.y) {
			glm::ivec2 workPixel(i + left, j + up);
			int dstOffset = count_img_offset(workPixel, dstWorkArea);

			if (dstOffset < 0)
				continue;

			double distance;
			glm::vec4 color;

			if (intersect(&trig, workPixel, distance, color) && distance > 0) {
				//dst_image[dstOffset] = glm::u8vec4(color);
				while (1 == atomicCAS(&mutex[dstOffset], 0, 1)) {
					
				}
				dst_image[dstOffset] = glm::u8vec4(color);
				if (distance < zbuffer[dstOffset]) {
					dst_image[dstOffset] = glm::u8vec4(color);
					zbuffer[dstOffset] = distance;
				}
				mutex[dstOffset] = 0;
			}
		}
	}
}

void mesh_paint2(const Triangle* mesh, int triangle_count, glm::u8vec4* dst_image, ImageWorkArea dstWorkArea, glm::uvec2 blockDim) {
	dim3 blkDim(blockDim.x, blockDim.y, 1);
	dim3 grdDim = { static_cast<unsigned int>(triangle_count), 1, 1 };
	
	int size = dstWorkArea.imageDim.x * dstWorkArea.imageDim.y;
	CudaResource<unsigned int> mutex(size * sizeof(unsigned int), ResourceLocation::Device);
	CudaResource<float> zbuffer(size * sizeof(float), ResourceLocation::Device);

	thrust::device_ptr<float> dev_ptr(zbuffer.getHandle());
	thrust::fill(dev_ptr, dev_ptr + size, INFINITY);
	cudaMemset(mutex.getHandle(), 0, size * sizeof(unsigned int));

	_mesh_paint2 << <grdDim, blkDim >> > (mesh, mutex.getHandle(), zbuffer.getHandle(), dst_image, dstWorkArea);

	cudaDeviceSynchronize();
	checkCudaErrors(cudaGetLastError());
}