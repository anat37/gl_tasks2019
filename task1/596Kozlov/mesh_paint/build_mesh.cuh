#pragma once

#include <glm/glm.hpp>
#include <common/common_functions.cuh>
#include "triangle.h"

void build_mesh(Triangle* mesh, int grid_x, int grid_z, glm::uvec2 blockDim);
